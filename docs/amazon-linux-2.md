# Docs for configuring minikube on amazon-linux-2

> __NOTE__: this not working for this moment. 
> 
> Need additional network configuration to allow minikube accept incoming
> traffic from public IP and forward it to pods 

## Table of content

<!-- toc -->



<!-- tocstop -->

Script for configuring and running minikube
```
sudo amazon-linux-extras install

# Install docker
sudo yum install docker
sudo systemctl enable docker
sudo systemctl restart docker
sudo chmod 666 /var/run/docker.sock

# Install minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

# Install kubectl
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
sudo yum install -y kubectl

# Run minikube
minikube start \
    --cpus=2 \
    --driver=docker \
    --addons=ingress \
    --apiserver-ips 23.21.194.143 \
    --ports=9090,9091,9092,9093,9094
# minikube addons enable ingress
# minikube addons enable istio metrics-server dashboard

# Install helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh


##### Alternative #####

# Installing and preparing kubeadm solution
cat > /etc/sysctl.d/20-bridge-nf.conf <<EOF
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system

# Configure docker
mkdir /etc/docker
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
sudo systemctl restart docker

sudo yum install kubelet kubeadm

kubeadm init --pod-network-cidr=10.0.101.0/24

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf /home/ec2-user/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

source <(kubectl completion bash)

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/a70459be0084506e4ec919aa1c114638878db11b/Documentation/kube-flannel.yml
kubectl get pods --all-namespaces

kubectl taint nodes --all node-role.kubernetes.io/control-plane:NoSchedule-

docker run -d \
    --name nginx \
    -p 8080:80 \
    -v /home/ec2-user/.minikube/profiles/minikube/client.key:/etc/nginx/certs/minikube-client.key \
    -v /home/ec2-user/.minikube/profiles/minikube/client.crt:/etc/nginx/certs/minikube-client.crt \
    -v /etc/nginx/conf.d/:/etc/nginx/conf.d \
    -v /etc/nginx/.htpasswd:/etc/nginx/.htpasswd \
nginx
```