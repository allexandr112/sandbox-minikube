# Docs for configuring microk8s on Ubuntu

## Table of content

<!-- toc -->

- [Initial configuration](#initial-configuration)
- [Applying letsencrypt cluster cert issuer](#applying-letsencrypt-cluster-cert-issuer)
- [Using cluster cert issuer in ingress](#using-cluster-cert-issuer-in-ingress)

<!-- tocstop -->

## Initial configuration
```
# Install snapd
sudo apt update
sudo apt install snapd

# Install kubectl
sudo apt-get update
sudo apt-get install -y ca-certificates curl
sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# Install helm
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

# Install microk8s and configure it for external api-server
sudo snap install microk8s --classic --channel=1.26
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
# Here need to relogin
microk8s status --wait-ready
cat << EOF >> /var/snap/microk8s/current/args/kube-apiserver
#/var/snap/microk8s/current/args/kube-apiserver
--advertise-address=54.145.72.20
EOF
# Add line 'IP.3 = 54.145.72.20' to the /var/snap/microk8s/current/certs/csr.conf.template
sudo microk8s refresh-certs --cert ca.crt
sudo microk8s refresh-certs --cert server.crt
sudo microk8s refresh-certs --cert front-proxy-client.crt
sudo snap restart microk8s
microk8s config > ~/.kube/config

# Installing microk8s addons
# https://microk8s.io/docs/addon-cert-manager
microk8s enable cert-manager ingress dns
```

## Applying letsencrypt cluster cert issuer
Apply the `lets-encrypt.yaml` manifest from [folder](../manifests):
```
kubectl apply -f lets-encrypt.yaml
```

## Using cluster cert issuer in ingress
Just add the annotation to your ingress after creating
[cluster cert issuer](##Applying letsencrypt cluster cert issuer):
```
cert-manager.io/cluster-issuer: lets-encrypt
```
or for development
```
cert-manager.io/cluster-issuer: lets-encrypt-stage
```