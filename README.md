# sandbox-minikube

## Table of content

<!-- toc -->

- [Running microk8s](#running-microk8s)
- [Why microk8s](#why-microk8s)

<!-- tocstop -->

## Running microk8s
Please follow [ubuntu documentation](docs/ubuntu.md) to perform initial setup of microK8s
with all required addons

## Why microk8s
My choice of microk8s is dictated by simplicity of it's installation on the host with
easy possibility to provide public access to services in the cluster. Previously I tried
to do the configuration of minikube and that required much more actions to provide access
to the services and also some changes in ip routing, which I didn't like. I spent quite a
time on that and decided to try another tools, as options there were kubeadm and microk8s,
I started from microk8s.

Alternative solutions like EKS and GCP I didn't use because of price 
(free-tier is used already)
Also they would require some time for configuration from perspective of IaC, because I would
need to define:
- Proper networking
- IAM roles
- EKS module
- Worker node groups